'use strict'

function getApiURL(endpoint) {
    return `/api/${endpoint}`
}

function signup(data) {
    return new Promise((resolve, reject) => {
        axios.post(getApiURL('users/signup'), data)
            .then(r => resolve(r.data))
            .catch(e => reject(e.response.data))
    })
}

function signin(data) {
    return new Promise((resolve, reject) => {
        axios.post(getApiURL('users/signin'), data)
            .then(r => resolve(r.data))
            .catch(e => reject(e.response.data))
    })
}

function getMovies() {
    return new Promise((resolve, reject) => {
        axios.get(getApiURL('movies'))
            .then(r => resolve(r.data))
            .catch(e => reject(e.response.data))
    })
}

function getMovie(id) {
    return new Promise((resolve, reject) => {
        axios.get(getApiURL(`movies/${id}`))
            .then(r => resolve(r.data))
            .catch(e => reject(e.response.data))
    })
}

function CreateMovie(data) {
    return new Promise((resolve, reject) => {
        axios.post(getApiURL(`movies/`), data, {
            header: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        })
            .then(r => resolve(r.data))
            .catch(e => reject(e.response.data))
    })
}


function GetGenres() {
    return new Promise((resolve, reject) => {
        axios.get(getApiURL('genres'))
            .then(r => resolve(r.data))
            .catch(e => reject(e.response.data))
    })
}

function GetMovieComments(movieID) {
    return new Promise((resolve, reject) => {
        axios.get(getApiURL(`movies/comments/${movieID}`))
            .then(r => resolve(r.data))
            .catch(e => reject(r.response.data))
    })
}

function postComment({ movie, comment }) {
    return new Promise((resolve, reject) => {
        const token = getAuthToken()

        axios({
            method: 'post',
            url: getApiURL('movies/comments/post'),
            data: { movie, comment },
            headers: {
                'Authorization': `Token ${token}`,
            }
          })
        .then(r => resolve(r.data))
        .catch(e => reject(e.response.data))
    })
}