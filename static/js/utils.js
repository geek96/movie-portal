function getAuthToken() {
  return localStorage.getItem("auth-token")
}

function setAuthToken(token) {
  localStorage.setItem('auth-token', token)
}

function removeToken() {
  localStorage.removeItem('auth-token')
}