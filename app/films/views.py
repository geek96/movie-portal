from django.shortcuts import render


def movies_list(request):
    return render(request, "movies.html")


def movie_detail(request, slug):
    return render(request, 'movie_detail.html', {'slug': slug})


def movie_create(request):
    return render(request, 'movie_create.html')