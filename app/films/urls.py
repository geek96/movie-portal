from django.urls import path, include

from .views import movies_list, movie_detail, movie_create

urlpatterns = [
    path('create', movie_create, name="create_movie"),
    path('<slug>', movie_detail, name='movies-detail'),
    path('', movies_list, name='movies-list'),
]
