from django.urls import path, include

urlpatterns = [
    path('films/', include('app.films.urls')),
]
