from django.conf.urls import url
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from .views import signup

urlpatterns = [
    url('signin', obtain_auth_token),
    path('signup', signup, name="api-signup")
]
