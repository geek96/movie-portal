from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient


class UserTestCase(TestCase):

    def setUp(self):
        """Define the test clients and other test variables"""

        self.test_user = User.objects.create_user(username='dummy', password='dummypassowrd')

        # URL for signup user.
        self.create_url = reverse('api-signup')
        self.client = APIClient()

    def test_api_create_user(self):
        """Ensure we can create a new user and a valid token is created with it."""
        data = {
            'username': 'dummy1',
            'password': 'somepasswd'
        }
        response = self.client.post(self.create_url, data, format='json')

        # We want to make sure we have two users in the database..
        self.assertEqual(User.objects.count(), 2)
        # And that we're returning a 201 created code.
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Additionally, we want to return the username upon successful creation.
        self.assertEqual(response.data['username'], data['username'])
        # Password should not be present in the response
        self.assertFalse('password' in response.data)

    def test_api_should_return_bad_request_on_long_username(self):
        data = {
            'username': 'dummy' * 50,
            'password': 'dummypassword'
        }
        response = self.client.post(self.create_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 1)

    def test_api_create_user_with_empty_username(self):
        data = {
            'username': '',
            'password': 'dummypassword'
        }

        response = self.client.post(self.create_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 1)

    def test_api_create_user_with_existing_username(self):
        data = {
            'username': 'dummy',
            'password': 'dummyuser'
        }

        response = self.client.post(self.create_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 1)

    def test_api_create_user_and_return_auth_token(self):
        """
        Ensure api can create a new user and a valid token is created with it.
        """
        data = {
            'username': 'dummy1',
            'password': 'somepasswd'
        }

        response = self.client.post(self.create_url, data, format='json')
        user = User.objects.latest('id')
        token = Token.objects.get(user=user)
        self.assertEqual(response.data['token'], token.key)

    def test_api_should_return_method_not_allowed_on_get_request(self):
        """Ensure api should not register user on get request"""
        data = {
            'username': 'dummy1',
            'password': 'somepasswd'
        }
        response = self.client.get(self.create_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(User.objects.count(), 1)
