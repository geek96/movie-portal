from rest_framework import viewsets, parsers

from api.movies.models import Movie
from api.movies.serializers import MovieSerializer


class MovieViewSet(viewsets.ModelViewSet):
    """This class defines the CRUD endpoints of movie api"""
    queryset = Movie.objects.order_by('-created_at')
    serializer_class = MovieSerializer
    parser_classes = (parsers.MultiPartParser, parsers.FormParser)
    lookup_field = 'slug'