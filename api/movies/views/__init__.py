from .movie import MovieViewSet
from .genre import GenreViewSet
from .comment import CommentCreateAPI, CommentsByMovieListAPI
