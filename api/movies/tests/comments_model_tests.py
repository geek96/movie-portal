from django.contrib.auth.models import User
from django.test import TestCase
from .test_helper import TestHelper
from api.movies.models import Movie, Comment


class CommentModelTestCase(TestCase):

    def setUp(self):
        """Setup test client and other test variables"""
        self.test_user = User.objects.create_user(username='dummy', password='dummypassowrd')
        self.movie_data = TestHelper.get_movie_data()
        self.movie_geners = TestHelper.get_movie_genres()
        self.test_movie = self.create_movie()

    def create_movie(self):
        """Helper method to create movie, set genre and return created movie object"""
        movie = Movie.objects.create(**self.movie_data)
        movie.genres.set(self.movie_geners)
        return movie

    def test_model_can_create_comment(self):
        """Test model can create comment"""
        Comment.objects.create(user=self.test_user, movie=self.test_movie, comment="test comment")
        self.assertEqual(Comment.objects.count(), 1)

    def test_model_get_comments_by_movie(self):
        """Test model can get comments by movie"""
        Comment.objects.create(user=self.test_user, movie=self.test_movie, comment="test comment")
        self.assertEqual(Comment.objects.filter(movie__id=self.test_movie.id).count(), 1)

    def test_model_get_comments_by_user(self):
        """Test model can get comments by user"""
        Comment.objects.create(user=self.test_user, movie=self.test_movie, comment="test comment")
        self.assertEqual(Comment.objects.filter(user__id=self.test_user.id).count(), 1)

    def test_model_get_comments_by_movie_and_user(self):
        """Test model can get comments by movie and user"""
        Comment.objects.create(user=self.test_user, movie=self.test_movie, comment="test comment")
        self.assertEqual(Comment.objects.filter(user__id=self.test_user.id, movie__id=self.test_movie.id).count(), 1)