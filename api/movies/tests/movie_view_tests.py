from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from django.urls import reverse

from .test_helper import TestHelper
from ..models import Movie


class MovieViewTestCase(TestCase):
    """Test suite for the model api views."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()
        self.movie_route_name = "movie-routes-list"
        self.url = reverse(self.movie_route_name)
        self.movie_data = {
            'genres': TestHelper.get_movie_genres(), **TestHelper.get_movie_data()}

    def api_request_create_movie(self):
        return self.client.post(
            self.url,
            self.movie_data,
            format="multipart"
        )

    def test_api_can_create_a_movie(self):
        """Test the api has movie creation capability"""
        response = self.api_request_create_movie()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Movie.objects.count(), 1)

    def test_api_return_error_on_duplicate_movie_name(self):
        """Test the api should return bad request on duplicate entry"""
        m1_response = self.api_request_create_movie()
        self.assertEqual(m1_response.status_code, status.HTTP_201_CREATED)
        m2_response = self.api_request_create_movie()
        self.assertEqual(m2_response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Movie.objects.count(), 1)

    def test_api_should_return_bad_request_on_long_movie_name(self):
        movie_data = self.movie_data
        movie_data["name"] = movie_data["name"] * 50
        response = self.client.post(
            self.url,
            movie_data,
            format="multipart"
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Movie.objects.count(), 0)

    def test_api_should_not_create_movie_without_photo(self):
        movie_data = self.movie_data
        del movie_data["photo"]
        response = self.client.post(
            self.url,
            movie_data,
            format="multipart"
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Movie.objects.count(), 0)

    def test_api_should_not_create_movie_without_name(self):
        movie_data = self.movie_data
        del movie_data["name"]
        response = self.client.post(
            self.url,
            movie_data,
            format="multipart"
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Movie.objects.count(), 0)

    def test_api_should_not_create_movie_without_multipart(self):
        self.assertRaises(Exception, self.client.post, self.url, self.movie_data, "json")
        self.assertEqual(Movie.objects.count(), 0)

    def test_api_should_not_create_movie_without_genres(self):
        movie_data = self.movie_data
        del movie_data["genres"]
        response = self.client.post(
            self.url,
            movie_data,
            format="multipart"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Movie.objects.count(), 0)