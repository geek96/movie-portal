from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from django.urls import reverse

from api.movies.models import Genre


class GenreViewTestCase(TestCase):
    """Test suite for the  genre model api views."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()
        self.genre_route_name = "genres-routes-list"
        self.url = reverse(self.genre_route_name)

    def api_reqeust_create(self):
        return self.client.post(
            self.url,
            {"name": "Drama"},
            format="json"
        )

    def test_api_can_create_a_genre(self):
        """Test the api has genre creation capability"""
        response = self.api_reqeust_create()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Genre.objects.count(), 1)

    def test_api_return_error_on_duplicate_genre_name(self):
        """Test the api should return bad request on duplicate entry"""
        g1 = self.api_reqeust_create()
        g2 = self.api_reqeust_create()
        self.assertEqual(g2.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Genre.objects.count(), 1)
