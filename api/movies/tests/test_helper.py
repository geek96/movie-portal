from django.core.files.uploadedfile import SimpleUploadedFile

from api.movies.models import Genre


class TestHelper:
    @staticmethod
    def get_movie_genres():
        genres = ['Drama', 'Thriller']
        genre_objs = []
        for genre in genres:
            g = Genre.objects.create(name=genre)
            genre_objs.append(g.id)
        return genre_objs

    @staticmethod
    def get_tmp_image():
        """Helper method to return the tmporary image file"""
        with open("test-data/default-movie-image.jpg", 'rb') as file:
            return file.read()
        return None

    @staticmethod
    def get_movie_data():
        """Helper method to return the movie dict"""
        image = TestHelper.get_tmp_image()
        photo = SimpleUploadedFile(
            'test_image.jpg', content=image, content_type='image/jpeg')

        return {
            'name': 'Jocker',
            'description': 'In Gotham City, mentally troubled comedian Arthur Fleck is disregarded and mistreated by society. He then embarks on a downward spiral of revolution and bloody crime. This path brings him face-to-face with his alter-ego: the Joker.',
            'release_date': '2019-05-01',
            'rating': 5,
            'ticket_price': 50,
            'country': 'Pakistan',
            'photo': photo
        }
