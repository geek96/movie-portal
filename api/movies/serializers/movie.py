from rest_framework import serializers
from api.movies.models import Movie


class MovieSerializer(serializers.ModelSerializer):
    """Serializer to map the Movie model instance into json format"""
    genres_list = serializers.StringRelatedField(
        many=True, read_only=True, source='genres')

    class Meta:
        """Meta class to map serializer's fields with the model fields"""
        model = Movie
        fields = ('id', 'name', 'slug', 'description', 'release_date', 'rating',
                  'ticket_price', 'price_currency', 'country', 'genres', 'genres_list', 'photo')
        read_only_fields = ('id', 'slug',)
        extra_kwargs = {'genres': {'write_only': True}}
