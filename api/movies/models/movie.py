from uuid import uuid4
from django.db import models
from django.core.validators import MinValueValidator as minVal, MaxValueValidator as maxVal
from django.utils.text import slugify

from .genre import Genre


class Movie(models.Model):
    """This class represents the movie model."""

    # Ticket price currency choices
    Currency = models.TextChoices('Currency', 'PKR')

    id = models.UUIDField(primary_key=True, default=uuid4)
    name = models.CharField(unique=True, max_length=255,
                            blank=False, null=False)
    slug = models.SlugField(unique=True, max_length=255,
                            blank=False, null=False)
    description = models.TextField(blank=False, null=False)
    release_date = models.DateField(blank=False, null=False)
    rating = models.PositiveSmallIntegerField(
        validators=[minVal(1), maxVal(5)])
    ticket_price = models.DecimalField(
        max_digits=6, decimal_places=2, blank=False, null=False, validators=[minVal(1)])
    price_currency = models.CharField(
        max_length=3, default='PKR', choices=Currency.choices)
    country = models.CharField(max_length=255, blank=False, null=False)
    genres = models.ManyToManyField(Genre)
    photo = models.ImageField(upload_to='movies/', blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "movies"

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return self.name

    def save(self, *args, **kwargs):
        """Overriding the save method, to slugify movie name"""
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)
