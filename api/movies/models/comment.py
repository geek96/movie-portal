from django.contrib.auth import get_user_model
from django.db import models
from uuid import uuid4

from api.movies.models import Movie


class Comment(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    comment = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'comments'