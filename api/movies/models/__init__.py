from .movie import Movie
from .genre import Genre
from .comment import Comment