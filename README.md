# Movie Portal

A simple web base movie portal, the aim of this project to provide a simple and easy interface to get movies information and share their thoughts.

## Features

- Film List
- Post new film
- Authentication (Token authentication)
- Post comment on film

## Technologies used

- Django
- Django Rest Framework
- Vuejs
- Axios
- Sqlite

## Setup

### Install dependencies

```bash
pip install -r requirements.txt

chmod +x ./manage.py
```

### Import data

```bash
./manage.py loaddata seed.json
```

### Run

```bash
./manage.py runserver
```

## Unit Tests

```bash
  ./manage.py test
```

### Coverage

```bash
  chmod +x runserver.sh
  ./runserver.sh
```

Total 28 test cases and 97% code coverage.
